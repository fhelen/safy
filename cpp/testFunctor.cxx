#include "otbCommandLineArgumentParser.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbImage.h"
#include "itkMacro.h"
#include "otbVectorImage.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionIteratorWithIndex.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>

#include "configParser.hxx"
#include "utils.hxx"

#include "otbSafyModelFunctor.h"

using C= const char*;
using S=std::string;
using D=double;
using V =vnl_vector<D>;
using VD =std::vector<D>;
using T=std::tuple<VD,VD,VD>;
using VariableVectorType = itk::VariableLengthVector<D>;

template<typename S,typename V,typename T> inline T readFile(const S &fileName){
        V doys,temp_air,r_g;
        std::ifstream  txt(fileName);
        std::string line;
        typedef decltype(doys) A;
        std::ifstream file(fileName);
        if (file.is_open()) {
                std::string line;
                while (getline(file, line)) {
                        std::stringstream  lineStream(line);
                        std::string  doy;
                        std::string  t_air;
                        std::string rg;
                        std::getline(lineStream,doy,' ');
                        std::getline(lineStream,t_air,' ');
                        std::getline(lineStream,rg,' ');
                        doys.push_back(boost::lexical_cast<typename A::value_type>(doy));
                        temp_air.push_back(boost::lexical_cast<typename A::value_type>(t_air));
                        r_g.push_back(boost::lexical_cast<typename A::value_type>(rg));
                }
                file.close();
        }
        return std::make_tuple(doys,temp_air,r_g);
}


int main(int argc, char * argv[])
{
  try
  {
    typedef otb::CommandLineArgumentParser ParserType;
    ParserType::Pointer parser = ParserType::New();
    parser->SetProgramDescription(
            "feature time series cluster -in inputImage -out outputImage");
    parser->AddInputImage();
    parser->AddOutputImage();
    typedef otb::CommandLineArgumentParseResult ParserResultType;
    ParserResultType::Pointer parseResult = ParserResultType::New();
    try
      {
              parser->ParseCommandLine(argc, argv, parseResult);
      }

    catch (itk::ExceptionObject& err)
      {
      std::string descriptionException = err.GetDescription();
      if (descriptionException.find("ParseCommandLine(): Help Parser")
          != std::string::npos)
        {
                return EXIT_SUCCESS;
        }
      if (descriptionException.find("ParseCommandLine(): Version Parser")
          != std::string::npos)
        {
                return EXIT_SUCCESS;
        }
      return EXIT_FAILURE;
      }

    typedef otb::VectorImage<float,2> VectorImageType;
    typedef otb::ImageFileReader<VectorImageType> ReaderType;
    typedef otb::ImageFileWriter<VectorImageType> WriterType;
    ReaderType::Pointer reader=ReaderType::New();
    VectorImageType::Pointer image = reader->GetOutput();
    WriterType::Pointer writer =WriterType::New();
    VectorImageType::Pointer outputVectorImage = VectorImageType::New();
    utils::LibUtils U;
    
    int noDataValue=-1;
    int featurePosition=0;
    
    typedef otb::Functor::SafyModelFunctor<itk::ImageRegionConstIteratorWithIndex<VectorImageType>,V, V, V,V,V,V,V,int> FunctorType;
    
    FunctorType SafyFunctor;

    S config="/home/fhelen/workspace/safy/cpp/configSafy.cfg";
    S scope="safy";
    parser::CfgParser<C> P(config.c_str(),scope.c_str());
    C fixedParamsScope="fixed";
    C optimParamsScope="optim";
    C initParamsScope="init";

    S dataFile="/home/fhelen/workspace/safy/cpp/climData.txt";
    auto data=readFile<S,VD,T>(dataFile);

    auto doys=std::get<0>(data);
    auto tempAir=std::get<1>(data);
    auto r_g=std::get<2>(data);

    auto fixedParams=P.getSafyParams<V,D>(fixedParamsScope);
    auto optimParams=P.getSafyParams<V,D>(optimParamsScope);
    auto initParams=P.getSafyParams<V,int>(initParamsScope);
    std::vector<double> dateVector={158, 161, 166, 167, 179, 188, 192, 193, 194, 199, 205, 209, 212, 219, 223, 225, 227, 231, 240, 245, 252, 272, 279, 292};

    
    auto doyVect=U.vectorToVnlVector<VD,V>(doys);
    auto tempAirVect=U.vectorToVnlVector<VD,V>(tempAir);
    auto rgVect=U.vectorToVnlVector<VD,V>(r_g);
    auto doySat=U.vectorToVnlVector<VD,V>(dateVector);

    reader->SetFileName(parseResult->GetInputImage().c_str());
    writer->SetFileName(parseResult->GetOutputImage().c_str());
   
    reader->Update();

    itk::ImageRegionConstIteratorWithIndex<VectorImageType> inputIterator(image,image->GetLargestPossibleRegion());
    
    
    for( inputIterator.GoToBegin() ; !inputIterator.IsAtEnd() ;  ++inputIterator){
            bool check=U.checkTSValidity<VariableVectorType>(inputIterator.Get());   
            if (check==true){
                    auto feature=SafyFunctor(inputIterator,fixedParams,optimParams,initParams,doyVect,tempAirVect,rgVect,doySat,featurePosition);
            }
    }
    
  }
  catch (itk::ExceptionObject& err)
    {
    std::cout << "Following otbException catch :" << std::endl;
    std::cout << err << std::endl;
    return EXIT_FAILURE;
    }
  catch (std::bad_alloc& err)
    {
    std::cout << "Exception bad_alloc : " << (char*) err.what() << std::endl;
    return EXIT_FAILURE;
    }
  catch (...)
    {
    std::cout << "Unknown Exception found !" << std::endl;
    return EXIT_FAILURE;
    }


        
  return EXIT_SUCCESS;
               
}

