#include "otbCommandLineArgumentParser.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbImage.h"
#include "itkMacro.h"
#include "otbVectorImage.h"
#include "otbSafyModelFilter.hxx"

int main(int argc, char * argv[])
{
  try
  {
    typedef otb::CommandLineArgumentParser ParserType;
    ParserType::Pointer parser = ParserType::New();
    parser->SetProgramDescription(
            "OtbSafyModelFilter -in inputImage -out outputImage -n -1 -f 1");
    parser->AddInputImage();
    parser->AddOutputImage();
    parser->AddOption("--noData",
                          "Set noData value " ,
                      "-n",
                      1,
                      true);
    parser->AddOption("--feat",
                          "Set feature Name  " ,
                      "-f",
                      1,
                      true);
    parser->AddOption("--configFile",
                          "config file path" ,
                      "-c",
                      1,
                      true);
    parser->AddOption("--scope",
                          "scope of config file ex: safy" ,
                      "-s",
                      1,
                      true);

    parser->AddOption("--fixed",
                          "fixed params scope" ,
                      "-s.f",
                      1,
                      true);
    parser->AddOption("--optim",
                          "optim params scope" ,
                      "-s.o",
                      1,
                      true);
    parser->AddOption("--init",
                          "init params scope" ,
                      "-s.i",
                      1,
                      true);
    parser->AddOption("--meteo",
                          "meteo file" ,
                      "-m",
                      1,
                      true);
    
    typedef otb::CommandLineArgumentParseResult ParserResultType;
    ParserResultType::Pointer parseResult = ParserResultType::New();
    try
      {
              parser->ParseCommandLine(argc, argv, parseResult);
      }

    catch (itk::ExceptionObject& err)
      {
      std::string descriptionException = err.GetDescription();
      if (descriptionException.find("ParseCommandLine(): Help Parser")
          != std::string::npos)
        {
                return EXIT_SUCCESS;
        }
      if (descriptionException.find("ParseCommandLine(): Version Parser")
          != std::string::npos)
        {
                return EXIT_SUCCESS;
        }
      return EXIT_FAILURE;
      }

    typedef otb::VectorImage<double,2> VectorImageType;
    typedef otb::ImageFileReader<VectorImageType> ReaderType;
    typedef otb::ImageFileWriter<VectorImageType> WriterType;
    ReaderType::Pointer reader=ReaderType::New();
    WriterType::Pointer writer =WriterType::New();
    VectorImageType::Pointer image = reader->GetOutput();
    VectorImageType::Pointer outputVectorImage = VectorImageType::New();

    typedef otb::SafyModelFilter<VectorImageType,VectorImageType> FilterType;
    
    FilterType::Pointer filter =FilterType::New();

    reader->SetFileName(parseResult->GetInputImage().c_str());
    writer->SetFileName(parseResult->GetOutputImage().c_str());
    reader->Update();
    
    filter->SetInput(reader->GetOutput());

    using C= char;
    using S=std::string;
    int  noData;
    
    
    S configPath,scope,fixed_scope,optim_scope,init_scope,feature,meteo;
    if (parseResult->IsOptionPresent("--configFile"))
            configPath=parseResult->GetParameterString("--configFile");
    if (parseResult->IsOptionPresent("--scope"))
            scope=parseResult->GetParameterString("--scope");
    if (parseResult->IsOptionPresent("--fixed"))
            fixed_scope=parseResult->GetParameterString("--fixed");
    if (parseResult->IsOptionPresent("--optim"))
            optim_scope=parseResult->GetParameterString("--optim");
    if (parseResult->IsOptionPresent("--init"))
            init_scope=parseResult->GetParameterString("--init");
    if (parseResult->IsOptionPresent("--feat"))
            feature=parseResult->GetParameterString("--feat");
    if (parseResult->IsOptionPresent("--noData"))
            noData=parseResult->GetParameterInt("--noData");
    if (parseResult->IsOptionPresent("--meteo"))
            meteo=parseResult->GetParameterString("--meteo");
    
    filter->SetConfigFile(configPath.c_str());
    filter->SetConfigScope(scope.c_str());
    filter->SetConfigFixedScope(fixed_scope.c_str());
    filter->SetConfigOptimScope(optim_scope.c_str());
    filter->SetConfigInitScope(init_scope.c_str());
    filter->SetNoData(noData);
    filter->SetFeature(feature);
    filter->SetMeteoFile(meteo);
    

    filter->Update();
    writer->SetInput(filter->GetOutput());
    writer->Update();
    
  }
  catch (itk::ExceptionObject& err)
    {
    std::cout << "Following otbException catch :" << std::endl;
    std::cout << err << std::endl;
    return EXIT_FAILURE;
    }
  catch (std::bad_alloc& err)
    {
    std::cout << "Exception bad_alloc : " << (char*) err.what() << std::endl;
    return EXIT_FAILURE;
    }
  catch (...)
    {
    std::cout << "Unknown Exception found !" << std::endl;
    return EXIT_FAILURE;
    }


        
  return EXIT_SUCCESS;
               
}

