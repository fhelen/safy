#ifndef otbSafyModelFilter_hxx
#define otbSafyModelFilter_hxx

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>

#include "itkImageToImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "itkImageRegionIteratorWithIndex.h"

#include "safyModelUtils.hxx"
#include "otbSafyModelFunctor.h"
#include "configParser.hxx"
#include "utils.hxx"


namespace otb{

        template <typename TInputImage,typename TOutputImage>
        class ITK_EXPORT SafyModelFilter :public itk::ImageToImageFilter<TInputImage,TOutputImage>
        {
        public:
                typedef SafyModelFilter Self;
                typedef itk::ImageToImageFilter<TInputImage,TOutputImage> Superclass;
                typedef itk::SmartPointer<Self> Pointer;
                typedef itk::SmartPointer<const Self> ConstPointer;

                itkNewMacro(Self);
                itkTypeMacro(SafyModelFilter,ImageToImageFilter);
                itkStaticConstMacro(ImageDimension,unsigned int,TInputImage::ImageDimension);

                typedef typename Superclass::InputImageType       InputImageType;
                typedef typename InputImageType::ConstPointer     InputImagePointer;
                typedef typename InputImageType::RegionType       InputImageRegionType;
                typedef typename InputImageType::PixelType        InputImagePixelType;
                typedef typename InputImageType::SizeType         InputImageSizeType;
                
                typedef typename Superclass::OutputImageType      OutputImageType;
                typedef typename OutputImageType::ConstPointer    OutputImagePointer;
                typedef typename OutputImageType::RegionType      OutputImageRegionType;
                typedef typename OutputImageType::PixelType       OutputImagePixelType;

                typedef typename InputImageType::InternalPixelType  InputImageInternalType;
                typedef typename OutputImageType::InternalPixelType OutputImageInternalType;

                typedef typename TInputImage::RegionType RegionType;
                typedef itk::ProcessObject ProcessObjectType;
                
                typedef vnl_vector<InputImageInternalType> V;
                typedef std::vector<InputImageInternalType> stdV;
                typedef std::string S;
                typedef const char* C;
                typedef std::map<S,int> M;
                typedef std::tuple<stdV,stdV,stdV,stdV> T;
                typedef itk::VariableLengthVector<InputImageInternalType> VariableLengthVectorType;
                
                typedef Functor::SafyModelFunctor<itk::ImageRegionConstIteratorWithIndex<InputImageType>,V,V,V,V,V,V,V,int> FunctorType;

                
                const TOutputImage * GetOutput() const;
                TOutputImage * GetOutput();
                
                void SetNoData(const int &noData);
                const int& GetNoData(void) const;

                void SetFeature(const S &feature);
                const S& GetFeature(void) const;

                void SetMeteoFile(const S &fileName);
                const S& GetMeteoFile(void) const;

                void SetConfigFile(C fileName);
                C GetConfigFile(void) const;
                
                void SetConfigScope(C scope);
                C GetConfigScope(void) const;
                
                void SetConfigFixedScope(C scope);
                C GetConfigFixedScope(void) const;
                
                void SetConfigOptimScope(C scope);
                C GetConfigOptimScope(void) const;

                void SetConfigInitScope(C scope);
                C GetConfigInitScope(void) const;
                
        protected:
                SafyModelFilter();
                ~SafyModelFilter() ITK_OVERRIDE;
                virtual void BeforeThreadedGenerateData() ITK_OVERRIDE;
                virtual void ThreadedGenerateData(const OutputImageRegionType& outputRegionFroThread,itk::ThreadIdType threadId) ITK_OVERRIDE;

        private:
                SafyModelFilter(const Self&);
                void operator =(const Self&);
                FunctorType m_functor;
                V m_fixedParams;
                V m_optimParams;
                V m_initParams;
                V m_doyModel;
                V m_tempAir;
                V m_rg;
                V m_doySat;
                int m_featurePosition;
                int m_noDataValue;
                C m_fixedParamScope;
                C m_optimParamScope;
                C m_initParamScope;
                C m_configScope;
                C m_configFileName;
                M m_dicoFeature;
                S m_featureName;
                S m_meteoFileName;
                
                parser::CfgParser<C>* m_Parser;
                utils::LibUtils* m_Utils;

                
                        

        };
}
#ifndef OTB_MANUAL_INSTANTIATION
#include "otbSafyModelFilter.txx"
#endif
#endif
