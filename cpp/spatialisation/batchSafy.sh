#!/bin/bash -e
dir=/home/fhelen/workspace/safy/cpp/spatialisation

features=(dam gla)
for element in "${features[@]}"
do
    outFile=$dir/"safy_10_"$element".tif"
    echo $outFile
    $dir/otbSafyModel -in $dir/timeSerieLAI_resample_10.tif  -out $outFile  -c $dir/configSafy.cfg -m $dir/meteo_data_daily.txt -s safy -s.o optim -s.f fixed -s.i init -f $element -n -1
done
