#ifndef otbSafyModelFunctor_h
#define otbSafyModelFunctor_h

#include "wrapperSafy.hxx"
#include "utils.hxx"

namespace otb
{
        namespace Functor
        {
                template<typename TIter,typename TFixedParams,typename TOptimParams,typename TInitParams,typename TDoyModel,typename TTempAir,typename TRg,typename TDoySat,typename TFeaturePosition,typename TNoData,typename TPrecision=double>
                        class SafyModelFunctor
                        {
                        public:
                        typedef SafyModelFunctor Self;
                        typedef TIter IteratorType;
                        typedef typename IteratorType::PixelType PixelType;
                        typedef TPrecision ScalarRealType;
                        typedef  itk::VariableLengthVector<ScalarRealType> OutputType;                        

                        SafyModelFunctor(){}
                        ~SafyModelFunctor(){}

                                inline OutputType operator()(TIter &it,TFixedParams &fixedParams,TOptimParams &optimParams,TInitParams &initParams,TDoyModel &doyModel,TTempAir &tempAir,TRg &rg,TDoySat &doySat,TFeaturePosition &featurePosition,TNoData &noData){
                                        OutputType feature;
                                        itk::VariableLengthVector<TPrecision> slice=it.Get();
                                        vnl_vector<TPrecision> pix(slice.GetDataPointer(),slice.GetSize());
                                        if(std::adjacent_find( pix.begin(), pix.end(), std::not_equal_to<TPrecision>() ) == pix.end() ) {
                                                feature.SetSize(doyModel.size());
                                                feature.Fill(noData);
                                        }
                                        else{
                                                auto tuple = safy::wrapper::safyWrapper<TFixedParams,TDoySat,TPrecision,std::tuple<vnl_vector<TPrecision>,vnl_vector<TPrecision>,vnl_vector<TPrecision>,vnl_vector<TPrecision>>>(fixedParams,optimParams,initParams,doyModel,tempAir,rg,doySat,pix);
                                                auto feat=safy::wrapper::get<vnl_vector<TPrecision>>(tuple,featurePosition);
                                                feature.SetData(feat.data_block(),feat.size(),false);
                                        }
                                        return feature;
                        }
                        };
        }
}
#endif
