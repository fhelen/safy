#ifndef _SAFYWRAPPER_HXX_
#define _SAFYWRAPPER_HXX_

#include "safyModel.hxx"
#include "safyCostFunction.hxx"

#define NOINLINE __attribute__((noinline))
#define ALWAYS_INLINE __attribute__((always_inline))

namespace safy{
namespace wrapper{

        template<typename T, typename U> struct return_ {
                static T& perform(U& u) NOINLINE {
                        throw std::runtime_error("return type mismatch");
                }
        };
        
        template<typename T> struct return_<T,T> {
                static T& perform(T& t) ALWAYS_INLINE { return t; }
        };

        template<typename Ret, typename Tuple, unsigned S> struct get_by_idx_ {
                static Ret& perform(Tuple& t, int idx) ALWAYS_INLINE {
                        constexpr unsigned C = S-1;
                        if(idx == C) {
                                typedef typename std::tuple_element<C,Tuple>::type elem_type;
                                return return_<Ret,elem_type>::perform(std::get<C>(t));
                        }
                        else {
                                return get_by_idx_<Ret,Tuple,C>::perform(t, idx);
                        }
                }
        };

        template<typename Ret, typename Tuple> struct get_by_idx_<Ret, Tuple, 0> {
                static Ret& perform(Tuple&, int) NOINLINE {
                        throw std::runtime_error("bad field index");   
                }
        };
        
        template<typename Ret, typename Tuple> Ret& get(Tuple& t, int idx) {
                constexpr unsigned S = std::tuple_size<Tuple>::value;
                return get_by_idx_<Ret,Tuple,S>::perform(t, idx);
        }

        template<typename V,typename VI,typename D,typename T> inline T safyWrapper(const V &fixedParams,const V &optimParams,const VI &initParams,const VI &doyModel,const V &tempAir,const V &rg,const VI &doySat,const V &glaSat){
                auto emergence=static_cast<unsigned int>(initParams[0]);
                auto harvest=static_cast<unsigned int>(initParams[1]);
                safy::SafyCostFunction<V,VI> CostFunction(optimParams.size(),doyModel,tempAir,rg,fixedParams,emergence,harvest,glaSat,doySat);
                V x=optimParams.as_ref();
                vnl_amoeba amoeba(CostFunction);
                amoeba.minimize(x);
                safy::Safy<V> S(fixedParams,x,emergence,harvest);
                S.wrapper(doyModel,tempAir,rg);
                return std::make_tuple(S.v_gla,S.v_frc,S.v_par,S.v_dam);
        }
}
}
#endif
