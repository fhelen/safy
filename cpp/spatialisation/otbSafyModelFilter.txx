#ifndef otbSafyModelFilter_txx
#define otbSafyModelFilter_txx

#include "otbSafyModelFilter.hxx"

namespace otb{

        template<typename TInputImage,typename TOutputImage>
        SafyModelFilter<TInputImage,TOutputImage>::SafyModelFilter(){
                m_dicoFeature.insert(std::pair<S,int>("gla",0));
                m_dicoFeature.insert(std::pair<S,int>("frc",1));
                m_dicoFeature.insert(std::pair<S,int>("par",2));
                m_dicoFeature.insert(std::pair<S,int>("dam",3));
        }

        template<typename TInputImage,typename TOutputImage>
        SafyModelFilter<TInputImage,TOutputImage>::~SafyModelFilter(){}

        template<typename TInputImage,typename TOutputImage>
        TOutputImage *
        SafyModelFilter<TInputImage,TOutputImage>::GetOutput(){
                return static_cast<TOutputImage *>(this->itk::ProcessObject::GetOutput(0));
        }

        template<typename TInputImage,typename TOutputImage>
        const TOutputImage *
        SafyModelFilter<TInputImage,TOutputImage>::GetOutput() const {
                return static_cast<TOutputImage *>(this->itk::ProcessObject::GetOutput(0));
        }


        template<typename TInputImage,typename TOutputImage>
        void
        SafyModelFilter<TInputImage,TOutputImage>::BeforeThreadedGenerateData(){
                m_configFileName=this->GetConfigFile();
                m_configScope=this->GetConfigScope();
                m_fixedParamScope=this->GetConfigFixedScope();
                m_optimParamScope=this->GetConfigOptimScope();
                m_initParamScope=this->GetConfigInitScope();
                m_featureName=this->GetFeature();
                m_noDataValue=this->GetNoData();
                m_featurePosition= m_dicoFeature.find(m_featureName)->second;
                m_meteoFileName=this->GetMeteoFile();
                m_Parser=new parser::CfgParser<C>(m_configFileName,m_configScope);
                m_Utils =new utils::LibUtils();
                m_fixedParams=m_Parser->template getSafyParams<V,InputImageInternalType>(m_fixedParamScope);
                m_optimParams=m_Parser->template getSafyParams<V,InputImageInternalType>(m_optimParamScope);
                m_initParams=m_Parser->template getSafyParams<V,InputImageInternalType>(m_initParamScope);
                auto tuple=safy::readFile<S,stdV,T>(m_meteoFileName);
                V doyModel(std::get<0>(tuple).data(),std::get<0>(tuple).size());
                V tempAir(std::get<1>(tuple).data(),std::get<1>(tuple).size());
                V rg(std::get<2>(tuple).data(),std::get<2>(tuple).size());
                auto temp=safy::extractDoys<stdV,int>(std::get<0>(tuple), std::get<3>(tuple), 1);
                V doySat(temp.data(),temp.size());
                m_doyModel=doyModel;
                m_tempAir=tempAir;
                m_rg=rg;
                m_doySat=doySat;
                unsigned int size=m_doyModel.size();
                this->GetOutput()->SetNumberOfComponentsPerPixel(size);
                this->GetOutput()->Allocate();
                this->GetOutput()->SetOrigin(this->GetInput()->GetOrigin());
                this->GetOutput()->SetSpacing(this->GetInput()->GetSpacing());
                this->GetOutput()->SetProjectionRef(this->GetOutput()->GetProjectionRef());
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetConfigFile(C fileName){
                m_configFileName=const_cast<C>(fileName);
        }

        template <typename TInputImage, typename TOutputImage>
        const char* SafyModelFilter<TInputImage,TOutputImage>::GetConfigFile(void) const{
                return (static_cast<C>(m_configFileName));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetConfigScope(C scope){
                m_configScope=const_cast<C>(scope);
        }

        template <typename TInputImage, typename TOutputImage>
        const char* SafyModelFilter<TInputImage,TOutputImage>::GetConfigScope(void) const{
                return (static_cast<C>(m_configScope));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetConfigFixedScope(C scope){
                m_fixedParamScope=const_cast<C>(scope);
        }

        template <typename TInputImage, typename TOutputImage>
        const char* SafyModelFilter<TInputImage,TOutputImage>::GetConfigFixedScope(void) const{
                return (static_cast<C>(m_fixedParamScope));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetConfigOptimScope(C scope){
                m_optimParamScope=const_cast<C>(scope);
        }

        template <typename TInputImage, typename TOutputImage>
        const char* SafyModelFilter<TInputImage,TOutputImage>::GetConfigOptimScope(void) const{
                return (static_cast<C>(m_optimParamScope));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetConfigInitScope(C scope){
                m_initParamScope=const_cast<C>(scope);
        }

        template <typename TInputImage, typename TOutputImage>
        const char* SafyModelFilter<TInputImage,TOutputImage>::GetConfigInitScope(void) const{
                return (static_cast<C>(m_initParamScope));
        }


        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetNoData(const int &noData){
                m_noDataValue=const_cast<int&>(noData);
        }

        template <typename TInputImage, typename TOutputImage>
        const int& SafyModelFilter<TInputImage,TOutputImage>::GetNoData(void) const{
                return (static_cast<const int&>(m_noDataValue));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetFeature(const S &feature){
                m_featureName=const_cast<S&>(feature);
        }

        template <typename TInputImage, typename TOutputImage>
        const std::string& SafyModelFilter<TInputImage,TOutputImage>::GetFeature(void) const{
                return (static_cast<const S&>(m_featureName));
        }

        template <typename TInputImage, typename TOutputImage>
        void SafyModelFilter<TInputImage,TOutputImage>::SetMeteoFile(const S &fileName){
                m_meteoFileName=const_cast<S&>(fileName);
        }

        template <typename TInputImage, typename TOutputImage>
        const std::string& SafyModelFilter<TInputImage,TOutputImage>::GetMeteoFile(void) const{
                return (static_cast<const S&>(m_meteoFileName));
        }
        
        template<typename TInputImage,typename TOutputImage>
        void
        SafyModelFilter<TInputImage,TOutputImage>
        ::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
        {
                itk::ImageRegionConstIteratorWithIndex<TInputImage> inputIterator(this->GetInput(),outputRegionForThread);
                itk::ImageRegionIteratorWithIndex<TOutputImage> outputIterator(this->GetOutput(),outputRegionForThread); 
                for(outputIterator.GoToBegin(), inputIterator.GoToBegin() ; !outputIterator.IsAtEnd() && !inputIterator.IsAtEnd() ; ++outputIterator, ++inputIterator){
                        VariableLengthVectorType feature=m_functor(inputIterator,m_fixedParams,m_optimParams,m_initParams,m_doyModel,m_tempAir,m_rg,m_doySat,m_featurePosition,m_noDataValue);
                        outputIterator.Set(feature);
                }

        }

}

#endif












