#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>
#include "configParser.hxx"
#include "utils.hxx"
// #include "safyModel.hxx"
// #include "safyCostFunction.hxx"
// #include "wrapperSafy.hxx"
#include <itkVariableLengthVector.h>
#include "otbSafyModelFunctor.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "safyModelUtils.hxx"

using C= const char*;
using S=std::string;
using D=double;
using M=std::map<S,D>;
using VD=std::vector<D>;
using T=std::tuple<VD,VD,VD,VD>;
using VnlD=vnl_vector<D>;
using I =int;


template<typename S,typename V,typename T> inline T readFile(const S &fileName){
        V doys,temp_air,r_g,sat_bool;
        std::ifstream  txt(fileName);
        std::string line;
        typedef decltype(doys) A;
        std::ifstream file(fileName);
        if (file.is_open()) {
                std::string line;
                while (getline(file, line)) {
                        std::stringstream  lineStream(line);
                        std::string  doy;
                        std::string  t_air;
                        std::string rg;
                        std::string satBool;
                        std::getline(lineStream,doy,' ');
                        std::getline(lineStream,t_air,' ');
                        std::getline(lineStream,rg,' ');
                        std::getline(lineStream,satBool,' ');
                        doys.push_back(boost::lexical_cast<typename A::value_type>(doy));
                        temp_air.push_back(boost::lexical_cast<typename A::value_type>(t_air));
                        r_g.push_back(boost::lexical_cast<typename A::value_type>(rg));
                        sat_bool.push_back(boost::lexical_cast<typename A::value_type>(satBool));
                }
                file.close();
        }
        return std::make_tuple(doys,temp_air,r_g,sat_bool);
}



int main(int argc, char *argv[])
{
        S config="/home/fhelen/workspace/safy/cpp/configSafy.cfg";
        S scope="safy";
        parser::CfgParser<C> P(config.c_str(),scope.c_str());
        C fixedParamsScope="fixed";
        C optimParamsScope="optim";
        C initParamsScope="init";
        utils::LibUtils U;
        auto fixedParams=P.getSafyParams<VnlD,D>(fixedParamsScope);
        auto optimParams=P.getSafyParams<VnlD,D>(optimParamsScope);
        auto initParams=P.getSafyParams<VnlD,I>(initParamsScope);

        S dataFile="/home/fhelen/workspace/safy/cpp/climDataSat.txt";
        auto data=readFile<S,VD,T>(dataFile);
        
        safy::Safy<VnlD> S(fixedParams,optimParams,initParams[0],initParams[1]);
        
        auto doys=std::get<0>(data);
        auto tempAir=std::get<1>(data);
        auto r_g=std::get<2>(data);

        auto doyVect=U.vectorToVnlVector<VD,VnlD>(doys);
        auto tempAirVect=U.vectorToVnlVector<VD,VnlD>(tempAir);
        auto rgVect=U.vectorToVnlVector<VD,VnlD>(r_g);
        

        S.wrapper<VD,VD>(doys,tempAir,r_g);
        int index=0;
        for (auto it = doys.begin(); it != doys.end(); ++it,index++) {
                std::cout <<*it<<' '<<S.v_dam[index]<<' '<<S.v_gla[index]<<' '<<S.v_elue[index]  << std::endl;
                
        }
        
        // VnlD satDoy{3};
        // satDoy[0]=155;
        // satDoy[1]=180;
        // satDoy[2]=200;

        // VnlD satVal{3};
        // satVal[0]=0.2;
        // satVal[1]=1.8;
        // satVal[2]=2.8;

        // auto tuple =safy::wrapper::safyWrapper<VnlD,VnlD,D,std::tuple<VnlD,VnlD,VnlD,VnlD>>(fixedParams, optimParams,initParams, doyVect, tempAirVect, rgVect, satDoy, satVal);

        // auto dam=safy::wrapper::get<VnlD>(tuple,3);
        

        // std::cout <<dam  << std::endl;
        
        

        // using VariableVectorType = itk::VariableLengthVector<double>;
        // VariableVectorType variableLengthVector;
        // VariableVectorType t;
        // variableLengthVector.SetSize(3);
        // // t.SetSize(3);
        
        // variableLengthVector[0] = 1.1;
        // variableLengthVector[1] = 2.2;
        // variableLengthVector[2] = 3.3;

        // std::cout <<variableLengthVector  << std::endl;
        // vnl_vector<D> c(variableLengthVector.GetDataPointer(),variableLengthVector.GetSize());
        // std::cout <<c  << std::endl;

        // std::ifstream file(dataFile);
        // file.unsetf(std::ios_base::skipws);
        // unsigned int line_count=std::count(std::istream_iterator<char>(file),std::istream_iterator<char>(),'\n');
        // std::cout <<line_count  << std::endl;
        
        // std::vector<double> a={1,2,3};
        // vnl_vector<double> test(a.data(),a.size());
        // std::cout <<test  << std::endl;        


        // auto dataT=safy::readFile<S,VD,T>(dataFile);
        // auto bordel=safy::extractDoys<VD,int>(std::get<0>(dataT), std::get<3>(dataT), 1);
        // vnl_vector<D> d(bordel.data(),bordel.size());
        // std::cout << d << std::endl;
        
        // t.SetData(c.data_block(),3,false);
        // std::cout <<t  << std::endl;

        
        // auto emergence=static_cast<unsigned int>(initParams[0]);
        // auto harvest=static_cast<unsigned int>(initParams[1]);
        
        // safy::SafyCostFunction<VnlD,VnlD> CostFunction(optimParams.size(),doyVect,tempAirVect,rgVect,fixedParams,emergence,harvest,satVal,satDoy);

        // VnlD x=optimParams.as_ref();
        // vnl_amoeba amoeba(CostFunction);

        // amoeba.minimize(x);

        // safy::Safy<VnlD> toto(fixedParams,x,initParams[0],initParams[1]);
        // toto.wrapper<VD,VD>(doys,tempAir,r_g);

        // for (auto it = doys.begin(); it != doys.end(); ++it,index++) {
        //         std::cout <<*it<<' '<<tempAir[index]<<' '<<r_g[index] <<' '<<std::get<0>(tuple)[index]<<' '<<std::get<1>(tuple)[index]<<' '<<std::get<2>(tuple)[index] << ' '<<std::get<3>(tuple)[index]  << std::endl;
        // }
        
        return 0;
}
