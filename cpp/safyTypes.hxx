#ifndef _SAFYMODELTYPES_
#define _SAFYMODELTYPES_
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "safyModelUtils.hxx"
#include "otbSafyModelFunctor.h"
#include "configParser.hxx"
#include "utils.hxx"


namespace safy{
        
        using C=const char*;
        using S=std::string;
        using VS=std::vector<S>;
        
}
#endif
