* Validation Safy
** GLA
#+begin_src gnuplot :var ref="/home/fhelen/workspace/safy/cpp/true.dat" :var test="/home/fhelen/workspace/safy/cpp/to_valid.dat" :results output :file /home/fhelen/workspace/safy/cpp/valid_GLA.png 
set xtic rotate
set title "GLA"
unset key
plot '$ref' u 1:4:xtic(1) w lp ls 1,'$test' u 1:4:xtic(1) w lp ls 3;
#end_src
** FRC
#+begin_src gnuplot :var ref="/home/fhelen/workspace/safy/cpp/true.dat" :var test="/home/fhelen/workspace/safy/cpp/to_valid.dat" :results output :file /home/fhelen/workspace/safy/cpp/valid_FRC.png 
set xtic rotate
set title "FRC"
unset key
plot '$ref' u 1:5:xtic(1) w lp ls 1,'$test' u 1:5:xtic(1) w lp ls 3;
#end_src
** PAR
#+begin_src gnuplot :var ref="/home/fhelen/workspace/safy/cpp/true.dat" :var test="/home/fhelen/workspace/safy/cpp/to_valid.dat" :results output  :file /home/fhelen/workspace/safy/cpp/valid_PAR.png 
set xtic rotate
set title "PAR"
unset key
plot '$ref' u 1:6:xtic(1) w lp ls 1,'$test' u 1:6:xtic(1) w lp ls 3;
#end_src
** DAM
#+begin_src gnuplot :var ref="/home/fhelen/workspace/safy/cpp/true.dat" :var test="/home/fhelen/workspace/safy/cpp/to_valid.dat" :results output  :file /home/fhelen/workspace/safy/cpp/valid_DAM.png 
set xtic rotate
set title "DAM"
unset key
plot '$ref' u 1:7:xtic(1) w lp ls 1,'$test' u 1:7:xtic(1) w lp ls 3;
#+end_src





