#ifndef _SAFYMODELUTILS_HXX_
#define _SAFYMODELUTILS_HXX_

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <boost/variant.hpp>


namespace safy{

        template<typename S,typename V,typename T> inline T readFile(const S &fileName){
                V doys,temp_air,r_g,sat_bool;
                std::ifstream  txt(fileName);
                std::string line;
                typedef decltype(doys) A;
                std::ifstream file(fileName);
                if (file.is_open()) {
                        std::string line;
                        while (getline(file, line)) {
                                std::stringstream  lineStream(line);
                                std::string  doy;
                                std::string  t_air;
                                std::string rg;
                                std::string satBool;
                                std::getline(lineStream,doy,' ');
                                std::getline(lineStream,t_air,' ');
                                std::getline(lineStream,rg,' ');
                                std::getline(lineStream,satBool,' ');
                                doys.push_back(boost::lexical_cast<typename A::value_type>(doy));
                                temp_air.push_back(boost::lexical_cast<typename A::value_type>(t_air));
                                r_g.push_back(boost::lexical_cast<typename A::value_type>(rg));
                                sat_bool.push_back(boost::lexical_cast<typename A::value_type>(satBool));
                        }
                        file.close();
                }
                return std::make_tuple(doys,temp_air,r_g,sat_bool);
        }

        template<typename V,typename T> inline V extractDoys(const V &doys,const V &ident,const T &val){
                V doySat;
                if(doys.size()!=ident.size())
                        itkGenericExceptionMacro(<< "vectors have differents sizes: " << doys.size() << "/" << ident.size()<< "\n");
                typename V::const_iterator iter = ident.begin();
                while ((iter = std::find(iter, ident.end(), val)) != ident.end())
                {
                        auto index = std::distance(ident.begin(), iter);
                        doySat.push_back(doys[index]);
                        iter++;
                }
                return doySat;
        }
}
#endif
