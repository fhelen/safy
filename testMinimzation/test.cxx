#include <stdlib.h>
#include <stdio.h>
#include <functional>
#include <algorithm>
#include <vnl/vnl_least_squares_function.h>
#include <vnl/vnl_sparse_lst_sqr_function.h>
#include <vnl/algo/vnl_levenberg_marquardt.h>
#include <vnl/algo/vnl_amoeba.h>
#include <vnl/vnl_cost_function.h>
#include <vnl/vnl_inverse.h>
#include <iostream>
#include <fstream>
#include "utils.hxx"

using V=vnl_vector<double>;

using FunctionType =std::function<double (const V&)>;

class CostFunction : public vnl_cost_function
{
        
public:
        CostFunction(unsigned int nbParams, FunctionType func):
                vnl_cost_function(nbParams), costF(std::move(func)) {};

        inline double f(const V &x) override {
                auto val=costF(x);
                return val;
        }

private:
        FunctionType costF;
        
};

inline double fv(const vnl_vector<double> x){
        const double t1 = (1 - x[0]);
        const double t2 = (x[1] - x[0] * x[0]);
        const double t3 = ((x[3]-x[2])*x[3]) ;
        return t1*t1+100*t2*t2;
}


int main(int argc, char *argv[])
{
        vnl_vector<double> x{3};
        x[0]=-1;
        x[1]=2;
        x[2]=10;
        std::cout <<x << "\n";
        auto tt(fv);
        CostFunction C(3,tt);
        vnl_amoeba O(C);
        O.minimize(x);
        std::cout <<x  << "\n";
        return 0;
}
